# Project Overview
This project is being developed to monitor the parameters of the Magnetic Resonance Imaging (MRI) Pump, which is used as the cooling system to the MRI machine.

The cooling system is one of the critical systems of MRI, so it becomes necessary to monitor the functioning(i.e. parameters) of Pump.

Pressure, temperature, noise, and vibration of the pump are the assessment parameters, and they are been measured periodically with the help of raspberry pi kits.

• Our project aim is to send the relevant data(parameters), measured at each MRI Pump to a central storage location.

• Depending on data received and send out alerts to corresponding and responsible persons.

• Providing a functionality to pull the data from the selected MRI when required.

• Provide an interface to visualize and analyze the data.

* [MySql database installation with workbench](https://gitlab.com/rapuru/mripumpmonitoring/wikis/Database-installation-and-set-up)
* [Project-architecture](https://gitlab.com/rapuru/mripumpmonitoring/wikis/Project-architecture)
* [Database details](https://gitlab.com/rapuru/mripumpmonitoring/wikis/database-details)
* [Python file details](https://gitlab.com/rapuru/mripumpmonitoring/wikis/python-file-details)
* [Raspberry pi set up](https://gitlab.com/rapuru/mripumpmonitoring/wikis/raspberry-pi-set-up)

